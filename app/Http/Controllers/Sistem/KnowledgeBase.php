<?php

namespace App\Http\Controllers\Sistem;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KnowledgeBase extends Controller
{
    public function faq(){
        return view('components.knowledgebase.faq');
    }
    public function petunjuk(){
        return view('components.knowledgebase.petunjuk_aplikasi');
    }
}
