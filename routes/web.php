<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Sistem\AdminSistem;
use App\Http\Controllers\Sistem\KnowledgeBase;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('dashboard-sistem');
});

Route::get('/faq', [KnowledgeBase::class, 'faq'])->name('faq');
Route::get('/petunjuk-aplikasi',[KnowledgeBase::class, 'petunjuk'])->name('petunjuk-aplikasi');
